﻿namespace MES.AutoPublish.Blazor.Db
{
    /// <summary>
    /// 数据库初始化
    /// </summary>
    public static class DbInitializer
    {
        public static void Initialize(PublicContext context)
        {
            context.Database.EnsureCreated();
            // Look for any students.
            //if (context.SysSettings.Any())
            //{
            //    return;   // DB has been seeded
            //}

            //context.SysSettings.Add(new SysSettingEntity { Id = 1 });
            //context.SaveChanges();

            //context.PublicBasics.Add(new PublicBasicEntity { Id = 1 });
            //context.SaveChanges();

            //context.PublicDetails.Add(new PublicDetailEntity { Id = 1 });
            //context.SaveChanges();

        }

        public static void CreateDbIfNotExists(IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<PublicContext>();
                    DbInitializer.Initialize(context);
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred creating the DB.");
                }
            }
        }
    }
}
