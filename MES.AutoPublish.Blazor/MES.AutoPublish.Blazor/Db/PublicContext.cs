﻿using MES.AutoPublish.Blazor.Models;
using Microsoft.EntityFrameworkCore;
using SkiaSharp;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace MES.AutoPublish.Blazor.Db
{
    public class PublicContext : DbContext
    {
        public PublicContext(DbContextOptions<PublicContext> options) : base(options)
        {
        }

        public DbSet<SysSettingEntity> SysSettings { get; set; }
        public DbSet<PublishBasicEntity> PublishBasics { get; set; }
        public DbSet<PublishDetailEntity> PublishDetails { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SysSettingEntity>().ToTable("SysSettings");
            modelBuilder.Entity<PublishBasicEntity>().ToTable("PublicBasic");
            modelBuilder.Entity<PublishDetailEntity>().ToTable("PublicDetail");
        }
    }
}
