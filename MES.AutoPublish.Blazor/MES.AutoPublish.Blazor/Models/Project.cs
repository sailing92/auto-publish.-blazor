﻿namespace MES.AutoPublish.Blazor.Models
{
    public class Project
    {
        public string File { get; set; }

        public string Name { get; set; }

        public string Path { get; set; }

        public List<Project> Children { get; set; }

    }
}
