﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace MES.AutoPublish.Blazor.Models
{
    /// <summary>
    /// 发布版本表
    /// </summary>
    [Table("PublicBasic")]
    public class PublishBasicEntity
    {
        /// <summary>
        /// 唯一标识
        /// </summary>
        [Key]
        public long Id { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column(TypeName = "datetime")]
        public DateTime BaseCreateTime { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string BaseCreator { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column(TypeName = "datetime")]
        public DateTime BaseModifyTime { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string? BaseModifier { get; set; }
        /// <summary>
        /// 应用Id
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string? ApplicationId { get; set; }
        /// <summary>
        /// 应用名称
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        [Required]
        public string? ApplicationName { get; set; }
        /// <summary>
        /// 应用名称
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        [Required]
        public string? ApplicationVersion { get; set; }
        /// <summary>
        /// 备份文件夹名
        /// </summary>
        [Column(TypeName = "varchar(500)")]
        public string? BackupDirName { get; set; }
        /// <summary>
        /// 发布内容说明
        /// </summary>
        [Column(TypeName = "varchar(500)")]
        [Required]
        public string? Description { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column(TypeName = "varchar(500)")]
        public string? Remarks { get; set; }
        /// <summary>
        /// 状态 0：待发布 1：已发布 -1：撤销 
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 回退说明
        /// </summary>
        [Column(TypeName = "varchar(500)")]
        public string? FallbackDescription { get; set; }
        [NotMapped]
        public List<string> Projects { get; set; }=new List<string>();
    }
}
