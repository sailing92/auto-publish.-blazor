﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace MES.AutoPublish.Blazor.Models
{
    /// <summary>
    /// 发布版本详情表
    /// </summary>
    [Table("PublicDetail")]
    public class PublishDetailEntity
    {
        /// <summary>
        /// 唯一标识
        /// </summary>
        [Key]
        public long Id { get; set; }
        /// <summary>
        /// 发布Id
        /// </summary>
        public long publishId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column(TypeName = "datetime")]
        public DateTime BaseCreateTime { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string BaseCreator { get; set; }
        /// <summary>
        /// 发包文件（自动按照发布文件夹结构进行更新）
        /// </summary>
        [Column(TypeName = "varchar(2000)")]
        public string? PublishFile { get; set; }
        /// <summary>
        /// 程序文件（如果有强制对应的文件，请设置此字段）
        /// </summary>
        [Column(TypeName = "varchar(2000)")]
        public string? ApplicationFile { get; set; }
    }
}
