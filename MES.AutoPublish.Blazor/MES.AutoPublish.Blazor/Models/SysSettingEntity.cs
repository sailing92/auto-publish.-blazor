﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace MES.AutoPublish.Blazor.Models
{
    /// <summary>
    /// 系统设置
    /// </summary>
    [Table("SysSettings")]
    public class SysSettingEntity
    {
        /// <summary>
        /// 唯一标识
        /// </summary>
        [Key]
        public long Id { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column(TypeName = "datetime")]
        public DateTime BaseCreateTime { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string BaseCreator { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column(TypeName = "datetime")]
        public DateTime BaseModifyTime { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string? BaseModifier { get; set; }
        /// <summary>
        /// 应用Id
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        [Required]
        public string? ApplicationId { get; set; }
        /// <summary>
        /// 应用名称
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        [Required]
        public string? ApplicationName { get; set; }
        /// <summary>
        /// 发布文件夹
        /// </summary>
        [Required]
        [Column(TypeName = "varchar(2000)")]
        public string? PublicDir { get; set; }
        /// <summary>
        /// 程序文件夹
        /// </summary>
        [Column(TypeName = "varchar(2000)")]
        [Required]
        public string? ApplicationDir { get; set; }
        /// <summary>
        /// 服务名称(可以为空)
        /// </summary>
        [Column(TypeName = "varchar(500)")]
        public string? ServiceName { get; set; }
        /// <summary>
        /// 程序端口
        /// </summary>
        [Range(0,65535)]
        public int ServicePort { get; set; }
        /// <summary>
        /// 邮箱设定（多账号以英文分号隔开(a.qq.com;b.qq.com）
        /// </summary>
        [Column(TypeName = "varchar(2000)")]
        public string? EmailSet { get; set; }

    }
}
