﻿using MES.AutoPublish.Blazor.Db;
using MES.AutoPublish.Blazor.Models;
using MES.AutoPublish.Blazor.Pages.App.Publish.ViewModel;
using MES.AutoPublish.Blazor.Pages.App.User;
using MES.AutoPublish.Blazor.Utils;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;
using Microsoft.EntityFrameworkCore;
using System.ServiceProcess;

namespace MES.AutoPublish.Blazor.Pages.App.Publish
{
    public partial class List
    {
        public bool _visible;
        public long _id;
        public PublishPage _PublishPage;
        private bool _overlay;
        private string _name;
        private List<int> _pageSizes = new() { 10, 25, 50, 100 };
        [Inject]
        public ProtectedLocalStorage ProtectedLocalStorage { get; set; } = default!;
        private readonly List<DataTableHeader<PublishBasicEntity>> _headers = new()
        {
            //new() { Text = "应用编码", Value = nameof(PublishBasicEntity.ApplicationId), CellClass = "" },
            new() { Text = "应用名称", Value = nameof(PublishBasicEntity.ApplicationName) },
            new() { Text = "版本号", Value = nameof(PublishBasicEntity.ApplicationVersion) },
            new() { Text = "备份文件夹名", Value = nameof(PublishBasicEntity.BackupDirName) },
            new() { Text = "发布内容说明", Value = nameof(PublishBasicEntity.Description) },
            new() { Text = "状态", Value = nameof(PublishBasicEntity.Status) },
            new() { Text = "操作", Value = "Action", Sortable = false }
        };

        protected override async Task OnInitializedAsync()
        {
            await LoadData();
        }

        private async Task LoadData()
        {
            using (PublicContext context = DbFactory.CreateDbContext())
            {
                List<PublishBasicEntity> Datas = await context.PublishBasics.OrderByDescending(t => t.BaseCreateTime).ToListAsync();
                _PublishPage = new PublishPage(Datas);
            }
        }


        protected override async Task OnAfterRenderAsync(bool firstRender) {
            if (firstRender)
            {
                _name = (await ProtectedLocalStorage.GetAsync<string>("UserName")).Value;
            }
        }


        private void NavigateToDetails(string id)
        {
            Nav.NavigateTo($"/app/user/view/{id}");
        }

        private void NavigateToEdit(string id)
        {
            Nav.NavigateTo($"/app/user/edit/{id}");
        }

        private async Task AddShow()
        {
            _id = 0;
            _visible = true;
        }

        private async Task Edit(long id)
        {
            _id = id;
            _visible = true;
        }

        private async Task Push(long id)
        {
            var confirm = await PopupService.ConfirmAsync("确认", "确认发布当前项吗？");
            if (confirm)
            {
                Loading(true);
                try
                {
                    using (PublicContext context = DbFactory.CreateDbContext())
                    {
                        #region 初始化
                        //类型标识  1：客户端 2：服务端
                        int publishType = 1;
                        //应用文件夹
                        string ApplicationDir = string.Empty;
                        //应用备份文件夹
                        string ApplicationBakDir = string.Empty;
                        //发布文件夹
                        string PublicDir = string.Empty;
                        var data = context.PublishBasics.Find(id);
                        SysSettingEntity sysSetting = context.SysSettings.Where(t => t.ApplicationId == data.ApplicationId).FirstOrDefault(); 
                        string ServiceName = sysSetting.ServiceName;
                        int Port = sysSetting.ServicePort;
                        //服务检查次数
                        int num = 0;

                        #endregion
                        if (data != null)
                        {
                            #region 发布
                            //确认更新类型,服务为空则为客户端更新，不为空则为服务端更新
                            if (!string.IsNullOrEmpty(sysSetting.ServiceName))
                            {
                                publishType = 2;
                            }

                            #region 1.文件备份
                            ApplicationDir = sysSetting.ApplicationDir;
                            PublicDir = sysSetting.PublicDir;
                            if (!Directory.Exists(ApplicationDir))
                            {
                                await PopupService.AlertAsync("程序文件夹不存在，请检查！", AlertTypes.Error);
                                return;
                            }
                            else
                            {
                                DirectoryInfo appinfo = new DirectoryInfo(ApplicationDir);
                                ApplicationBakDir = appinfo.Parent.FullName + Path.DirectorySeparatorChar + data.ApplicationVersion;
                                Directory.CreateDirectory(ApplicationBakDir);
                                if (!Directory.Exists(ApplicationBakDir))
                                {
                                    await PopupService.AlertAsync("备份文件夹创建失败，请检查！", AlertTypes.Error);
                                    return;
                                }
                                else
                                {
                                    FileHelper.CopyDirectory(ApplicationDir, ApplicationBakDir);
                                }
                            }

                            #endregion
                            #region 2.文件覆盖
                            if (publishType==2)
                            {
                                ServiceController sc = new ServiceController(ServiceName);
                                if (sc.Status == ServiceControllerStatus.Running)
                                {
                                    sc.Stop();
                                    while (sc.Status == ServiceControllerStatus.Running)
                                    {
                                        Thread.Sleep(1000);
                                        sc.Refresh();
                                        num++;
                                        if (num>11)
                                        {
                                            break;
                                        }
                                    }
                                }
                                if (sc.Status != ServiceControllerStatus.Stopped)
                                {
                                    await PopupService.AlertAsync("无法关闭服务，请检查！", AlertTypes.Error);
                                    return;
                                }
                            }
                            var detaildata = context.PublishDetails.Where(t => t.publishId == id).ToList();
                            foreach (var item in detaildata)
                            {
                                File.Copy(item.PublishFile, item.PublishFile.Replace(PublicDir, ApplicationDir),true);
                            }
                            #endregion
                            #region 3.脚本执行
                            if (publishType==1)
                            {
                                if (!CmdHelper.RunCmd(ApplicationDir+Path.DirectorySeparatorChar+ "CreateAutoUpdateXml.bat"))
                                {
                                    await PopupService.AlertAsync("版本更新脚本执行异常，请回退！", AlertTypes.Error);
                                    return;
                                }
                            }
                            else if (publishType==2)
                            {
                                ServiceController sc = new ServiceController(ServiceName);
                                if (sc.Status == ServiceControllerStatus.Stopped)
                                {
                                    sc.Start();
                                }

                                while (sc.Status != ServiceControllerStatus.Running)
                                {
                                    Thread.Sleep(1000);
                                    sc.Refresh();
                                    num++;
                                    if (num > 11)
                                    {
                                        break;
                                    }
                                }

                                if (sc.Status!= ServiceControllerStatus.Running)
                                {
                                    await PopupService.AlertAsync("重启服务异常，请回退！", AlertTypes.Error);
                                    return;
                                }

                                if (Port!=0)
                                {
                                    if (!PortHelper.PortInUse(Port))
                                    {
                                        await PopupService.AlertAsync("服务端口检查异常，请回退！", AlertTypes.Error);
                                        return;
                                    }
                                }
                            }
                            #endregion
                            #endregion
                            data.Status = 1;
                            context.PublishBasics.Update(data);
                        }
                        context.SaveChanges();
                    }
                    await PopupService.AlertAsync("发布当前项成功！", AlertTypes.Success);
                }
                catch (Exception ex)
                {
                    await PopupService.AlertAsync("发布当前项失败！异常信息："+ ex.Message, AlertTypes.Error);
                }
                finally
                {
                    Loading(false);
                }
            }
            _id = 0;
            _visible = false;
            await LoadData();
        }

        private async Task Undo(long id)
        {
            var confirm = await PopupService.ConfirmAsync("确认", "确认回退当前项吗？");
            if (confirm)
            {
                Loading(true);
                try
                {
                    using (PublicContext context = DbFactory.CreateDbContext())
                    {
                        //初始化
                        //类型标识  1：客户端 2：服务端
                        int publishType = 1;
                        //应用文件夹
                        string ApplicationDir = string.Empty;
                        //应用备份文件夹
                        string ApplicationBakDir = string.Empty;
                        //发布文件夹
                        string PublicDir = string.Empty;
                        //发布内容信息
                        var data = context.PublishBasics.Find(id);
                        //应用信息
                        SysSettingEntity sysSetting = context.SysSettings.Where(t => t.ApplicationId == data.ApplicationId).FirstOrDefault();
                        ApplicationDir = sysSetting.ApplicationDir;
                        PublicDir = sysSetting.PublicDir;
                        //服务名
                        string ServiceName = sysSetting.ServiceName;
                        //端口
                        int Port = sysSetting.ServicePort;
                        //服务检查次数
                        int num = 0;
                        if (data != null)
                        {
                            if (data.Status != 0)
                            {
                                #region 回退
                                //确认更新类型,服务为空则为客户端更新，不为空则为服务端更新
                                if (!string.IsNullOrEmpty(sysSetting.ServiceName))
                                {
                                    publishType = 2;
                                }
                                #region 1.确认备份
                                DirectoryInfo appinfo = new DirectoryInfo(ApplicationDir);
                                ApplicationBakDir = appinfo.Parent.FullName + Path.DirectorySeparatorChar + data.ApplicationVersion+ Path.DirectorySeparatorChar+ appinfo.Name;
                                if (!Directory.Exists(ApplicationBakDir))
                                {
                                    await PopupService.AlertAsync("备份文件夹未找到，请检查！", AlertTypes.Error);
                                    return;
                                }
                                #endregion

                                #region 2.文件还原覆盖
                                if (publishType == 2)
                                {
                                    ServiceController sc = new ServiceController(ServiceName);
                                    if (sc.Status == ServiceControllerStatus.Running)
                                    {
                                        sc.Stop();
                                        while (sc.Status == ServiceControllerStatus.Running)
                                        {
                                            Thread.Sleep(1000);
                                            sc.Refresh();
                                            num++;
                                            if (num > 11)
                                            {
                                                break;
                                            }
                                        }
                                    }
                                    if (sc.Status != ServiceControllerStatus.Stopped)
                                    {
                                        await PopupService.AlertAsync("无法关闭服务，请检查！", AlertTypes.Error);
                                        return;
                                    }
                                }
                                var detaildata = context.PublishDetails.Where(t => t.publishId == id).ToList();
                                foreach (var item in detaildata)
                                {
                                    var a = item.PublishFile.Replace(PublicDir, ApplicationBakDir);
                                    var b = item.PublishFile.Replace(PublicDir, ApplicationDir);

                                    File.Copy(item.PublishFile.Replace(PublicDir, ApplicationBakDir), item.PublishFile.Replace(PublicDir, ApplicationDir), true);
                                }
                                #endregion

                                #region 3.脚本执行
                                if (publishType == 1)
                                {
                                    if (!CmdHelper.RunCmd(ApplicationDir + Path.DirectorySeparatorChar + "CreateAutoUpdateXml.bat"))
                                    {
                                        await PopupService.AlertAsync("版本更新脚本执行异常！", AlertTypes.Error);
                                        return;
                                    }
                                }
                                else if (publishType == 2)
                                {
                                    ServiceController sc = new ServiceController(ServiceName);
                                    if (sc.Status == ServiceControllerStatus.Stopped)
                                    {
                                        sc.Start();
                                    }

                                    while (sc.Status != ServiceControllerStatus.Running)
                                    {
                                        Thread.Sleep(1000);
                                        sc.Refresh();
                                        num++;
                                        if (num > 11)
                                        {
                                            break;
                                        }
                                    }

                                    if (sc.Status != ServiceControllerStatus.Running)
                                    {
                                        await PopupService.AlertAsync("重启服务异常！", AlertTypes.Error);
                                        return;
                                    }

                                    if (Port != 0)
                                    {
                                        if (!PortHelper.PortInUse(Port))
                                        {
                                            await PopupService.AlertAsync("服务端口检查异常！", AlertTypes.Error);
                                            return;
                                        }
                                    }
                                }
                                #endregion

                                #endregion
                                data.Status = -1;
                                context.PublishBasics.Update(data);
                            }
                            else
                            {
                                await PopupService.AlertAsync("当前项尚未发布，无法回退！", AlertTypes.Error);
                                return;
                            }

                        }
                        context.SaveChanges();
                    }
                    await PopupService.AlertAsync("回退当前项成功！", AlertTypes.Success);
                }
                catch (Exception ex)
                {
                    await PopupService.AlertAsync("回退当前项失败！异常信息：" + ex.Message, AlertTypes.Error);
                }
                finally
                {
                    Loading(false);
                }
            }
            _id = 0;
            _visible = false;
            await LoadData();
        }

        private async Task Del(long id)
        {
            var confirm = await PopupService.ConfirmAsync("确认", "确认删除此发布吗？");
            if (confirm)
            {
                try
                {
                    using (PublicContext context = DbFactory.CreateDbContext())
                    {
                        var data = context.PublishBasics.Find(id);
                        if (data != null)
                        {
                            context.PublishBasics.Remove(data);
                        }
                        context.SaveChanges();
                    }
                    await PopupService.AlertAsync("删除发布信息成功！", AlertTypes.Success);
                }
                catch (Exception ex)
                {
                    await PopupService.AlertAsync("删除发布信息失败！异常信息："+ ex, AlertTypes.Error);
                }
            }
            _id = 0;
            _visible = false;
            await LoadData();
        }
        private async Task Add(PublishBasicEntity Data)
        {
            try
            {
                if (Data.Id == 0)
                {
                    using (PublicContext context = DbFactory.CreateDbContext())
                    {
                        if (context.PublishBasics.Any(t => t.ApplicationVersion == Data.ApplicationVersion && t.ApplicationId == Data.ApplicationId))
                        {
                            await PopupService.AlertAsync("发布版本已存在，请检查调整！", AlertTypes.Error);
                            return;
                        }
                        Data.Id = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
                        Data.BaseCreator = _name;
                        Data.BaseCreateTime = DateTime.Now;
                        Data.BaseModifyTime = DateTime.Now;
                        context.PublishBasics.Add(Data);
                        if (Data.Projects.Count > 0)
                        {
                            List<PublishDetailEntity> lst = new List<PublishDetailEntity>();
                            foreach (var item in Data.Projects)
                            {
                                PublishDetailEntity detailEntity = new PublishDetailEntity();
                                detailEntity.publishId = Data.Id;
                                detailEntity.BaseCreateTime = DateTime.Now;
                                detailEntity.BaseCreator = _name;
                                detailEntity.PublishFile = item;
                                lst.Add(detailEntity);
                            }
                            context.AddRange(lst);
                        }
                        context.SaveChanges();
                    }
                    await PopupService.AlertAsync("插入发布信息成功！", AlertTypes.Success);
                }
                else
                {
                    using (PublicContext context = DbFactory.CreateDbContext())
                    {
                        #region 更新基础表
                        context.PublishBasics.Attach(Data);
                        context.PublishBasics.Entry(Data).State = EntityState.Modified;
                        Data.BaseModifyTime = DateTime.Now;
                        Data.BaseModifier = _name;
                        context.Entry(Data).Property("BaseCreator").IsModified = false;
                        context.Entry(Data).Property("BaseCreateTime").IsModified = false;
                        context.Entry(Data).Property("Id").IsModified = false;
                        #endregion
                        #region 更新详细表

                        List<PublishDetailEntity> lstDetail = context.PublishDetails.Where(t => t.publishId == Data.Id).ToList();
                        context.RemoveRange(lstDetail);
                        if (Data.Projects.Count > 0)
                        {
                            List<PublishDetailEntity> lst = new List<PublishDetailEntity>();
                            foreach (var item in Data.Projects)
                            {
                                PublishDetailEntity detailEntity = new PublishDetailEntity();
                                detailEntity.publishId = Data.Id;
                                detailEntity.BaseCreateTime = DateTime.Now;
                                detailEntity.BaseCreator = _name;
                                detailEntity.PublishFile = item;
                                lst.Add(detailEntity);
                            }
                            context.AddRange(lst);
                        }
                        #endregion
                        context.SaveChanges();
                    }
                    await PopupService.AlertAsync("更新发布信息成功！", AlertTypes.Success);
                }


            }
            catch (Exception ex)
            {
                await PopupService.AlertAsync("处理发布信息失败！", AlertTypes.Error);
            }
            _id = 0;
            _visible = false;
            await LoadData();
        }

        private bool _loading = false;

        private async void Loading(bool val)
        {
            _loading = val;
            _overlay = val;
            await InvokeAsync(StateHasChanged);
        }
    }
}
