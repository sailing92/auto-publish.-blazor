﻿using MES.AutoPublish.Blazor.Models;

namespace MES.AutoPublish.Blazor.Pages.App.Publish.ViewModel
{
    public class PublishPage
    {
        public List<PublishBasicEntity> Datas { get; set; }

        public string? Search { get; set; }

        public int PageIndex { get; set; } = 1;

        public int PageSize { get; set; } = 10;

        public int PageCount => (int)Math.Ceiling(CurrentCount / (double)PageSize);

        public int CurrentCount => GetFilterDatas().Count();

        public PublishPage(List<PublishBasicEntity> datas)
        {
            Datas = new List<PublishBasicEntity>();
            Datas.AddRange(datas);
        }

        private IEnumerable<PublishBasicEntity> GetFilterDatas()
        {
            IEnumerable<PublishBasicEntity> datas = Datas;

            if (Search is not null)
            {
                datas = datas.Where(d => d.ApplicationName.Contains(Search, StringComparison.OrdinalIgnoreCase) || d.ApplicationId.Contains(Search, StringComparison.OrdinalIgnoreCase)||d.Description.Contains(Search, StringComparison.OrdinalIgnoreCase) == true);
            }
            if (datas.Count() < (PageIndex - 1) * PageSize) PageIndex = 1;

            return datas;
        }

        public List<PublishBasicEntity> GetPageDatas()
        {
            return GetFilterDatas().Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
    }
}
