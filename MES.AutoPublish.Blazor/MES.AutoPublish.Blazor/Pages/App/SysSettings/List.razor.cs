﻿using MES.AutoPublish.Blazor.Db;
using MES.AutoPublish.Blazor.Models;
using MES.AutoPublish.Blazor.Pages.App.SysSettings.ViewModel;
using MES.AutoPublish.Blazor.Pages.App.User;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;
using Microsoft.EntityFrameworkCore;

namespace MES.AutoPublish.Blazor.Pages.App.SysSettings
{
    public partial class List
    {
        private bool _visible;
        private long _id;
        private SysSettingPage _sysSettingPage;
        private string _name=string.Empty;
        private List<int> _pageSizes = new() { 10, 25, 50, 100 };
        [Inject]
        public ProtectedLocalStorage ProtectedLocalStorage { get; set; } = default!;
        private readonly List<DataTableHeader<SysSettingEntity>> _headers = new()
        {
            new() { Text = "应用编码", Value = nameof(SysSettingEntity.ApplicationId), CellClass = "" },
            new() { Text = "应用名称", Value = nameof(SysSettingEntity.ApplicationName) },
            new() { Text = "发布文件夹", Value = nameof(SysSettingEntity.PublicDir) },
            new() { Text = "程序文件夹", Value = nameof(SysSettingEntity.ApplicationDir) },
            new() { Text = "服务名称", Value = nameof(SysSettingEntity.ServiceName) },
            new() { Text = "服务端口", Value = nameof(SysSettingEntity.ServicePort) },
            new() { Text = "操作", Value = "Action", Sortable = false }
        };

        protected override async Task OnInitializedAsync()
        {
            await LoadData();
        }

        private async Task LoadData() {
            using (PublicContext context = DbFactory.CreateDbContext())
            {
                List<SysSettingEntity> Datas = await context.SysSettings.OrderByDescending(t => t.BaseCreateTime).ToListAsync();
                _sysSettingPage = new SysSettingPage(Datas);
            }
        }


        private void NavigateToDetails(string id)
        {
            Nav.NavigateTo($"/app/user/view/{id}");
        }

        private void NavigateToEdit(string id)
        {
            Nav.NavigateTo($"/app/user/edit/{id}");
        }

        private async Task Detail(long id) {
            _id= id;
            _visible = true;
        }

        private async Task AddShow() {
            _id = 0;
            _visible = true;
        }

        private async Task Edit(long id) {
            _id = id;
            _visible = true;
        }

        private async Task Del(long id)
        {
            var confirm= await PopupService.ConfirmAsync("确认","确认删除此应用吗？");
            if (confirm)
            {
                try
                {
                    using (PublicContext context = DbFactory.CreateDbContext())
                    {
                        var data = context.SysSettings.Find(id);
                        if (data != null)
                        {
                            context.SysSettings.Remove(data);
                        }
                        context.SaveChanges();
                    }
                    await PopupService.AlertAsync("删除应用信息成功！", AlertTypes.Success);
                }
                catch (Exception)
                {
                    await PopupService.AlertAsync("删除应用信息失败！", AlertTypes.Error);
                }
            }
            _id = 0;
            _visible = false;
            await LoadData();
        }
        private async Task Add(SysSettingEntity Data)
        {
           
            try
            {
                _name = (await ProtectedLocalStorage.GetAsync<string>("UserName")).Value;
                if (Data.Id==0) {
                    using (PublicContext context = DbFactory.CreateDbContext())
                    {
                        var checkdata = context.SysSettings.Where(t => t.ApplicationId == Data.ApplicationId || t.ApplicationName == Data.ApplicationName).ToList();
                        if (checkdata.Count>0)
                        {
                            await PopupService.AlertAsync("已存在应用信息，请检查应用编码、应用名称！", AlertTypes.Error);
                            return;
                        }

                        Data.BaseCreator= _name;
                        Data.BaseCreateTime = DateTime.Now;
                        Data.BaseModifyTime = DateTime.Now;
                        context.SysSettings.Add(Data);
                        context.SaveChanges();
                    }
                    await PopupService.AlertAsync("插入应用信息成功！", AlertTypes.Success);
                }
                else
                {
                    using (PublicContext context = DbFactory.CreateDbContext())
                    {
                        context.SysSettings.Attach(Data);
                        context.SysSettings.Entry(Data).State= EntityState.Modified;
                        Data.BaseModifyTime = DateTime.Now;
                        Data.BaseModifier = _name;
                        context.Entry(Data).Property("BaseCreator").IsModified = false;
                        context.Entry(Data).Property("BaseCreateTime").IsModified = false;
                        context.Entry(Data).Property("Id").IsModified = false;
                        context.SaveChanges();
                    }
                    await PopupService.AlertAsync("更新应用信息成功！", AlertTypes.Success);
                }


            }
            catch (Exception ex)
            {
                await PopupService.AlertAsync("处理应用信息失败！", AlertTypes.Error);
            }
            _id = 0;
            _visible = false;
            await LoadData();
        }
    }
}
