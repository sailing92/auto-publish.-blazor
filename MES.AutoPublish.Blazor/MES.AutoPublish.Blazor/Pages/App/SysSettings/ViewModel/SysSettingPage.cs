﻿using MES.AutoPublish.Blazor.Models;
using System.Data;
using System.Numerics;

namespace MES.AutoPublish.Blazor.Pages.App.SysSettings.ViewModel
{
    public class SysSettingPage
    {
        public List<SysSettingEntity> Datas { get; set; }

        public string? Search { get; set; }

        public int PageIndex { get; set; } = 1;

        public int PageSize { get; set; } = 10;

        public int PageCount => (int)Math.Ceiling(CurrentCount / (double)PageSize);

        public int CurrentCount => GetFilterDatas().Count();

        public SysSettingPage(List<SysSettingEntity> datas)
        {
            Datas = new List<SysSettingEntity>();
            Datas.AddRange(datas);
        }

        private IEnumerable<SysSettingEntity> GetFilterDatas()
        {
            IEnumerable<SysSettingEntity> datas = Datas;

            if (Search is not null)
            {
                datas = datas.Where(d => d.ApplicationName.Contains(Search, StringComparison.OrdinalIgnoreCase) || d.ApplicationId?.Contains(Search, StringComparison.OrdinalIgnoreCase) == true);
            }

            if (datas.Count() < (PageIndex - 1) * PageSize) PageIndex = 1;

            return datas;
        }

        public List<SysSettingEntity> GetPageDatas()
        {
            return GetFilterDatas().Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }
    }
}
