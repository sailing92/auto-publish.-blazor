﻿using Masa.Blazor.Presets;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;

namespace MES.AutoPublish.Blazor.Pages.Authentication.Components
{
    public partial class Login
    {
        private bool _show;
        private string _name;
        private string _password;

        [Inject]
        public NavigationManager Navigation { get; set; } = default!;

        [Inject]
        public ProtectedLocalStorage ProtectedLocalStorage { get; set; } = default!;

        [Parameter]
        public bool HideLogo { get; set; }

        [Parameter]
        public double Width { get; set; } = 410;

        [Parameter]
        public StringNumber? Elevation { get; set; }

        [Parameter]
        public string CreateAccountRoute { get; set; } = $"pages/authentication/register-v1";

        [Parameter]
        public string ForgotPasswordRoute { get; set; } = $"pages/authentication/forgot-password-v1";
        public async Task OnLogin(MouseEventArgs args)
        {
            if (string.IsNullOrEmpty(_password))
            {
                await PopupService.AlertAsync("请输入密码！", AlertTypes.Warning);
            }
            else
            {
                if (_password == "admin")
                {
                    await ProtectedLocalStorage.SetAsync("UserName", _name);
                    await ProtectedLocalStorage.SetAsync("IsLogined", true);
                    Navigation.NavigateTo("Welcome/Welcome");
                }
                else
                {
                    await PopupService.AlertAsync("密码错误，请重新输入！", AlertTypes.Warning);
                }
            }
        }
        public async Task OnLogin(KeyboardEventArgs args)
        {
            
            if (args.Code =="Enter")
            {
                if (string.IsNullOrEmpty(_password))
                {
                    await PopupService.AlertAsync("请输入密码！", AlertTypes.Warning);
                }
                else
                {
                    if (_password == "admin")
                    {
                        await ProtectedLocalStorage.SetAsync("UserName", _name);
                        await ProtectedLocalStorage.SetAsync("IsLogined", true);
                        Navigation.NavigateTo("Welcome/Welcome");
                    }
                    else
                    {
                        await PopupService.AlertAsync("密码错误，请重新输入！", AlertTypes.Warning);
                    }
                }
            }
        }
        public void OnValueChanged(string val)
        {
            _password = val;
        }
        public void OnNameValueChanged(string val) { 
            _name= val;
        }
    }
}