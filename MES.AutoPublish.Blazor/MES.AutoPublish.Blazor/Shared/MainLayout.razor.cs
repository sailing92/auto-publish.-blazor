﻿using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;

namespace MES.AutoPublish.Blazor.Shared
{
    public partial class MainLayout
    {

        [Inject]
        public NavigationManager NavigationManager { get; set; } = default!;


        [Inject]
        public ProtectedLocalStorage ProtectedLocalStorage { get; set; } = default!;



        /// <summary>
        /// 每次呈现组件后调用的方法
        /// </summary>
        /// <param name="firstRender">true如果这是第一次OnAfterRender(Boolean)在此组件实例上调用，则设置为 ;否则为 false。</param>
        /// <returns></returns>
        protected override async Task OnAfterRenderAsync(bool firstRender) {
            if (firstRender)
            {
                var isLogined = false;
                isLogined = (await ProtectedLocalStorage.GetAsync<bool>("IsLogined")).Value;
                if (isLogined!=true)
                {
                    NavigationManager.NavigateTo("/pages/authentication/Login", true);
                }
            }
        }
    }
}
