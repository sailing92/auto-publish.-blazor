﻿using System.Diagnostics;

namespace MES.AutoPublish.Blazor.Utils
{
    public class CmdHelper
    {
        public static bool RunCmd(string path)
        {
            bool result = false;
            try
            {
                using (Process pro = new Process())
                {
                    FileInfo file = new FileInfo(path);
                    pro.StartInfo.WorkingDirectory = file.Directory.FullName;
                    pro.StartInfo.FileName = path;
                    pro.StartInfo.CreateNoWindow = true;
                    pro.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    pro.Start();
                    pro.WaitForExit(1000);
                    result = true;
                }
            }
            catch
            {

            }
            return result;
        }
    }
}
