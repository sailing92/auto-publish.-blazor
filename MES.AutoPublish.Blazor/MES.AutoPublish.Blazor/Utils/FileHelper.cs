﻿using MES.AutoPublish.Blazor.Models;

namespace MES.AutoPublish.Blazor.Utils
{
    public class FileHelper
    {
        public static List<Project> getFileName(List<Project> list, string filepath)
        {
            DirectoryInfo root = new DirectoryInfo(filepath);
            foreach (FileInfo f in root.GetFiles().OrderByDescending(t=>t.LastWriteTime))
            {
                list.Add(new Project
                {
                    Path=f.FullName,
                    Name = f.Name,
                    File = f.Extension
                });
            }
            return list;
        }

        public static List<Project> GetallDirectory(List<Project> list, string path)
        {
            DirectoryInfo root = new DirectoryInfo(path);
            var dirs = root.GetDirectories();
            if (dirs.Count() != 0)
            {
                foreach (DirectoryInfo d in dirs)
                {
                    list.Add(new Project
                    {
                        Path = d.FullName,
                        Name = d.Name,
                        File = null,
                        Children = GetallDirectory(new List<Project>(), d.FullName)
                    });
                }
            }
            list = getFileName(list, path);
            return list;
        }

        public static void CopyDirectory(string sourcePath, string destPath)
        {
            string floderName = Path.GetFileName(sourcePath);
            DirectoryInfo di = Directory.CreateDirectory(Path.Combine(destPath, floderName));
            string[] files = Directory.GetFileSystemEntries(sourcePath);

            foreach (string file in files)
            {
                if (Directory.Exists(file))
                {
                    CopyDirectory(file, di.FullName);
                }
                else
                {
                    File.Copy(file, Path.Combine(di.FullName, Path.GetFileName(file)), true);
                }
            }
        }
    }
}
