﻿global using BlazorComponent;
global using BlazorComponent.I18n;
global using Masa.Blazor;
global using MES.AutoPublish.Blazor.Data.App.ECommerce;
global using MES.AutoPublish.Blazor.Data.App.ECommerce.Dto;
global using MES.AutoPublish.Blazor.Data.App.Invoice;
global using MES.AutoPublish.Blazor.Data.App.Invoice.Dto;
global using MES.AutoPublish.Blazor.Data.App.Todo;
global using MES.AutoPublish.Blazor.Data.App.Todo.Dto;
global using MES.AutoPublish.Blazor.Data.App.User;
global using MES.AutoPublish.Blazor.Data.App.User.Dto;
global using MES.AutoPublish.Blazor.Data.Base;
global using MES.AutoPublish.Blazor.Data.Dashboard.Analytics;
global using MES.AutoPublish.Blazor.Data.Dashboard.ECommerce;
global using MES.AutoPublish.Blazor.Data.Dashboard.ECommerce.Dto;
global using MES.AutoPublish.Blazor.Data.Others.AccountSettings;
global using MES.AutoPublish.Blazor.Data.Others.AccountSettings.Dto;
global using MES.AutoPublish.Blazor.Data.Shared.Favorite;
global using MES.AutoPublish.Blazor.Global;
global using MES.AutoPublish.Blazor.Global.Config;
global using MES.AutoPublish.Blazor.Global.Nav.Model;
global using Microsoft.AspNetCore.Components;
global using Microsoft.AspNetCore.Components.Forms;
global using Microsoft.AspNetCore.Components.Web;
global using Microsoft.AspNetCore.Http;
global using System.ComponentModel;
global using System.ComponentModel.DataAnnotations;
global using System.Globalization;
global using System.Net.Http.Json;
global using System.Reflection;
global using System.Text.Json;
